define("app", 	[	"custom", "angular", 
					"ui.router", "ngAnimate", "ngSanitize", "angular-centered", "sticky", "contenteditable", "mm.foundation", 
					"ui.utils", "psResponsive", "angular-carousel", "FBAngular", "chieffancypants.loadingBar", "dcbImgFallback",
					"stBlurredDialog", "SmoothScroll","adaptive.detection", "ngStorage", "toaster", "firebase", "string", "angularSpinkit",
					"gd.ui.jsonexplorer", "urish.load", "adaptive.youtube", "angularify.angular-lazyload", "angularFileUpload", "truncate",
					"uiModalCtrl", "accountModalCtrl", "checklist-model", "formly", "ngClickSelect", "multi-select",
					"smartTable.table", "ngPatternRestrict", "ngIdle", "angular-underscore"
				], function(custom) { 
	custom.logger("Function : app");
	
	
	////////////////
	// INIT 
	var app = angular.module('app', [	"ui.router", "ngAnimate", "ngSanitize", "angular-centered", "sticky", "contenteditable", "mm.foundation", 
										"ui.utils", "psResponsive", "angular-carousel", "FBAngular", "chieffancypants.loadingBar", "dcbImgFallback",
										"stBlurredDialog", "SmoothScroll","adaptive.detection", "ngStorage", "toaster", "firebase", "string", "angularSpinkit",
										"gd.ui.jsonexplorer", "urish.load", "adaptive.youtube", "angularify.angular-lazyload", "angularFileUpload", "truncate",
										"uiModalCtrl", "accountModalCtrl", "checklist-model", "formly", "ngClickSelect", "multi-select",
										"smartTable.table", "ngPatternRestrict", "ngIdle", "angular-underscore"
									]);
										
	app.init = function () {
		
		custom.setDefaultUserData(function(){
			angular.bootstrap(document, ['app']);	
		});
		 
		           
	};
	////////////////
	
	// let's make a nav called `myOffCanvas`
	app.factory('myOffCanvas', function (cnOffCanvas) {
	  return cnOffCanvas({
	    controller: 'headerCtrl',
	    templateUrl: 'components/offcanvas/offcanvas.html'
	  });
	}).
	
	// typically you'll inject the offCanvas service into its own
	// controller so that the nav can toggle itself
	controller('MyOffCanvasCtrl', function (myOffCanvas) {
	  this.toggle = myOffCanvas.toggle;
	}).
		
		
	directive('styleParent', function(){ 
	   return {
	     restrict: 'A',
	     link: function(scope, elem, attr) {
	         elem.on('load', function() {
	            var w = $(this).width(),
	                h = $(this).height();
				
					//$('#mainSlider').css('min-height', h + 'px');
					//$('#headerFiller').css('height', h + 'px'  ) ;
	            //check width and height and apply styling to parent here.
	         });
	     }
	   };
	}).	
		
	////////////////  CONFIG
	//
	config(function($stateProvider, $urlRouterProvider) {
			
			// For any unmatched url, redirect to /state1
			$urlRouterProvider.otherwise("/home");
		  
			// Now set up the states
			$stateProvider
			    .state('home', {
			      url: "/home?lang",
			      views: {
			        "overlay": { templateUrl: "layout/overlay/overlay.html",
			        			controller: "overlayController"
			        },				      				      	
			        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
			        			controller: "offcanvasController"
			        },				        
			        "ui-modals": { templateUrl: "layout/modals/ui/ui-modals.html"
			        			
			        },	
			        "account-modals": { templateUrl: "layout/modals/account/account-modals.html"
			        			
			        },				        			        			      	
			        "header": { templateUrl: "components/header/header.html",
			        			controller: "headerController"
			        },
			        "footer": { templateUrl: "components/footer/footer.html",
			        			controller: "footerController"
			        },			        			       
			        "body": { 	templateUrl: "components/home/home.html",
			        			controller: "homeController"
			         },			         
			      },	
				  onEnter: function(){
				   custom.transitionStart();
				  },
				  onExit: function(){
				    custom.transitionEnd();
				  },				  			      		      
			      
			    })			    
			    
			    .state('about', {
			      url: "/about?lang",
			      views: {
			        "overlay": { templateUrl: "layout/overlay/overlay.html",
			        			controller: "overlayController"
			        },				      				      	
			        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
			        			controller: "offcanvasController"
			        },	
			        "ui-modals": { templateUrl: "layout/modals/ui/ui-modals.html"
			        			
			        },	
			        "account-modals": { templateUrl: "layout/modals/account/account-modals.html"
			        			
			        },						        			        			        			        			      	
			        "header": { templateUrl: "components/header/header.html",
			        			controller: "headerController"
			        },
			        "footer": { templateUrl: "components/footer/footer.html",
			        			controller: "footerController"
			        },			        			       
			        "body": { 	templateUrl: "components/page2/page2.html",
			        			controller: "page2Controller"
			         },
			      },
			      onEnter: function(){
				   custom.transitionStart();
				  },
				  onExit: function(){
				    custom.transitionEnd();
				  },
			    })
			    
			    .state('service', {
			      url: "/service?lang",
			      views: {
			        "overlay": { templateUrl: "layout/overlay/overlay.html",
			        			controller: "overlayController"
			        },				      				      	
			        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
			        			controller: "offcanvasController"
			        },	
			        "ui-modals": { templateUrl: "layout/modals/ui/ui-modals.html"
			        			
			        },	
			        "account-modals": { templateUrl: "layout/modals/account/account-modals.html"
			        			
			        },						        			        			        			        			      	
			        "header": { templateUrl: "components/header/header.html",
			        			controller: "headerController"
			        },
			        "footer": { templateUrl: "components/footer/footer.html",
			        			controller: "footerController"
			        },			        			       
			        "body": { 	templateUrl: "components/page3/page3.html",
			        			controller: "page3Controller"
			         },
			      },
				  onEnter: function(){
				   custom.transitionStart();
				  },
				  onExit: function(){
				    custom.transitionEnd();
				  },			      	
			    })
			    
			    .state('apply', {
			      url: "/apply?lang",
			      views: {
			        "overlay": { templateUrl: "layout/overlay/overlay.html",
			        			controller: "overlayController"
			        },				      				      	
			        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
			        			controller: "offcanvasController"
			        },	
			        "ui-modals": { templateUrl: "layout/modals/ui/ui-modals.html"
			        			
			        },	
			        "account-modals": { templateUrl: "layout/modals/account/account-modals.html"
			        			
			        },						        			        			        			        			      	
			        "header": { templateUrl: "components/header/header.html",
			        			controller: "headerController"
			        },
			        "footer": { templateUrl: "components/footer/footer.html",
			        			controller: "footerController"
			        },			        			       
			        "body": { 	templateUrl: "components/page4/page4.html",
			        			controller: "page4Controller"
			         },
			      },
				  onEnter: function(){
				   custom.transitionStart();
				  },
				  onExit: function(){
				    custom.transitionEnd();
				  },			      	
			    })
			    
			    .state('events', {
			      url: "/events?lang",
			      views: {
			        "overlay": { templateUrl: "layout/overlay/overlay.html",
			        			controller: "overlayController"
			        },				      				      	
			        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
			        			controller: "offcanvasController"
			        },	
			        "ui-modals": { templateUrl: "layout/modals/ui/ui-modals.html"
			        			
			        },	
			        "account-modals": { templateUrl: "layout/modals/account/account-modals.html"
			        			
			        },						        			        			        			        			      	
			        "header": { templateUrl: "components/header/header.html",
			        			controller: "headerController"
			        },
			        "footer": { templateUrl: "components/footer/footer.html",
			        			controller: "footerController"
			        },			        			       
			        "body": { 	templateUrl: "components/page5/page5.html",
			        			controller: "page5Controller"
			         },
			      },
				  onEnter: function(){
				   custom.transitionStart();
				  },
				  onExit: function(){
				    custom.transitionEnd();
				  },			      	
			    })

			    .state('page6', {
			      url: "/page6",
			      views: {
			        "overlay": { templateUrl: "layout/overlay/overlay.html",
			        			controller: "overlayController"
			        },				      				      	
			        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
			        			controller: "offcanvasController"
			        },	
			        "ui-modals": { templateUrl: "layout/modals/ui/ui-modals.html"
			        			
			        },	
			        "account-modals": { templateUrl: "layout/modals/account/account-modals.html"
			        			
			        },							        			        			        			        			      	
			        "header": { templateUrl: "components/header/header.html",
			        			controller: "headerController"
			        },
			        "footer": { templateUrl: "components/footer/footer.html",
			        			controller: "footerController"
			        },			        			       
			        "body": { 	templateUrl: "components/page6/page6.html",
			        			controller: "page6Controller"
			         },
			      },
				  onEnter: function(){
				   custom.transitionStart();
				  },
				  onExit: function(){
				    custom.transitionEnd();
				  },			      	
			    });		     
			    			    			     
			    
			    			    
			    

	    			       
		});
		//
		////////////////   
	
	return app;
	
});
