define(['custom', 'papaParse'], function(custom) {

	
	var fileName  = 'page2';
  	custom.logger(fileName + "Controller: Init");
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			custom.logger(fileName + "Controller: Loaded");
				app.controller(fileName + 'Controller', function($scope, $stateParams, $location, $timeout, $localStorage, cfpLoadingBar) {	   
				   
				  
				    
				    
				    $scope.defaultSettings = $localStorage.$default({});
				    
					// INIT
					$scope.init = function(){
						
						// language default
						var isLang = $stateParams.lang;
						if (isLang == "chinese"){
							$scope.defaultSettings.language = "chinese";
						}	
						if (isLang == "english"){
							$scope.defaultSettings.language = "english";
						}					
						
						cfpLoadingBar.start();		
						custom.backstretchBG("media/images/clubInfoBG.jpg", 0, 1000);
						$scope.defaultLanguage = $scope.defaultSettings.language;
						custom.parseFile("files/exampleSpreadSheet.csv", function(data){							
							$scope.languageObj = data[$scope.defaultLanguage];
							$scope.$apply();
						});
								
								 
					};	

					$scope.changeLanguage = function(language){
							$scope.defaultSettings.language = language;							
							$stateParams.lang = language;
							currentUrl = document.URL; 
							
							if(currentUrl.indexOf('?') === -1){
								freshUrl = currentUrl;
							}
							else{
								freshUrl = currentUrl.substring(0, currentUrl.indexOf('?'));
							}							
						
							$timeout(function(){
								location.href = freshUrl;								
								location.reload();
							}, 250);
																					
					};

				    // fake the initial load so first time users can see it right away:
				    $timeout(function() {
				      cfpLoadingBar.complete();
				    }, 750);
				   

				});		
	    },
	    ///////////////////////////////////////
  };
});
