define(['custom', 'papaParse'], function(custom) {

	
	var fileName  = 'page5';
  	custom.logger(fileName + "Controller: Init");
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			custom.logger(fileName + "Controller: Loaded");
				app.controller(fileName + 'Controller', function($scope, $timeout, $localStorage, cfpLoadingBar) {	   
				   
				
				    
				    
				    $scope.defaultSettings = $localStorage.$default({});
				    
					// INIT
					$scope.init = function(){
						cfpLoadingBar.start();		
						custom.backstretchBG("media/images/applyBG.jpg", 0, 1000);
						$scope.defaultLanguage = $scope.defaultSettings.language;
						custom.parseFile("files/exampleSpreadSheet.csv", function(data){							
							$scope.languageObj = data[$scope.defaultLanguage];
							$scope.$apply();
						});
								
								 
					};	


						$scope.changeLanguage = function(language){
							$scope.defaultSettings.language = language;
							$timeout(function(){
								location.reload();
							}, 250);
																						
						};

				    // fake the initial load so first time users can see it right away:
				    $timeout(function() {
				      cfpLoadingBar.complete();
				    }, 750);
				   

				});		
	    },
	    ///////////////////////////////////////
  };
});
