define(['custom', 'papaParse'], function(custom) {

	
	var fileName  = 'page3';
  	custom.logger(fileName + "Controller: Init");
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			custom.logger(fileName + "Controller: Loaded");
				app.controller(fileName + 'Controller', function($scope, $timeout, $localStorage, cfpLoadingBar) {	   
				   
				   	   
				    

				    $scope.defaultSettings = $localStorage.$default({});
				    
					// INIT
					$scope.init = function(){
						cfpLoadingBar.start();		
						custom.backstretchBG("media/images/serviceBG.jpg", 0, 1000);	
						$scope.defaultLanguage = $scope.defaultSettings.language;
						custom.parseFile("files/exampleSpreadSheet.csv", function(data){							
							$scope.languageObj = data[$scope.defaultLanguage];
							$scope.$apply();
						});
								
								 
					};	
					
					$scope.changeLanguage = function(language){
						$scope.defaultSettings.language = language;
						$timeout(function(){
							location.reload();
						}, 250);
																					
					};					
					
					$scope.activeTab = function(active){
						$scope.tab1Active = false;
				    	$scope.tab2Active = false;
				    	$scope.tab3Active = false; 
				    	$scope.tab4Active = false; 
				    	$scope.tab5Active = false; 
				    	$scope.tab6Active = false;
				    	$scope.tab7Active = false;
				    	$scope['tab' + active + 'Active'] = true;
					};
					$scope.activeTab(1);
					
					
			

					
				    // fake the initial load so first time users can see it right away:
				    $timeout(function() {
				      cfpLoadingBar.complete();
				    }, 750);
				   

				});		
	    },
	    ///////////////////////////////////////
  };
});
