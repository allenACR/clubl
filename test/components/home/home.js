define(['custom', 'papaParse'], function(custom) {

	
	var fileName  = 'home';
  	custom.logger(fileName + "Controller: Init");
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			custom.logger(fileName + "Controller: Loaded");
				app.controller(fileName + 'Controller', function($scope, $timeout, $interval, $localStorage, cfpLoadingBar) {	   
				   
				  
				    
				    
				    $scope.defaultSettings = $localStorage.$default({});
				    
					// INIT
					$scope.init = function(){
						cfpLoadingBar.start();		
						custom.backstretchBG("media/images/homeBG.jpg", 0, 1000);
						$scope.defaultLanguage = $scope.defaultSettings.language;
						custom.parseFile("files/exampleSpreadSheet.csv", function(data){							
							$scope.languageObj = data[$scope.defaultLanguage];
							$scope.$apply();
						});
						
						$scope.changeLanguage = function(language){
							$scope.defaultSettings.language = language;
							$timeout(function(){
								location.reload();
							}, 250);
																						
						};
						
						var count1 = 1;
						var count2 = 2; 
						$scope.currentImage1 = "media/fadeIn/" + count1 + ".jpg";
						$scope.currentImage2 = "media/fadeIn/" + count2 + ".jpg";
						$scope.toggle = false;
					
						
						
						function updateImage() {

							
															
							if ($scope.toggle){
							count1 += 2;
							if (count1 > 11){
								count1 = 1;
							}	
								$('#topImg').fadeOut(1500);
								$('#btmImg').fadeIn(1500);
								
								var t1 = $timeout(function(){									
									updateImage1();					
								}, 1500);
																			
								
							}
							else{
							count2 += 2;
							if (count2 > 11){
								count2 = 2;
							}	
								$('#topImg').fadeIn(1500);
								$('#btmImg').fadeOut(1500);
								
								var t2 = $timeout(function(){
									updateImage2();		
								}, 1500);
																			
							}
							$scope.toggle = !$scope.toggle;
						}
						
						function updateImage1() {
							$scope.currentImage1  = "media/fadeIn/" + count1 + ".jpg";	
						}
						function updateImage2() {
							$scope.currentImage2  = "media/fadeIn/" + count2 + ".jpg";		
						}
												
						updateImages = $interval(updateImage, 5000);
	 
					};	
					
					
                  $scope.$on(
                        "$destroy",
                        function( event ) {
						   //$timeout.cancel( t1 );
                           //$timeout.cancel( t2 );
						   $interval.cancel(updateImages);
                        }
                    );
					
				    // fake the initial load so first time users can see it right away:
				    $timeout(function() {
				      cfpLoadingBar.complete();
				    }, 750);
				   

				});		
	    },
	    ///////////////////////////////////////
  };
});
