define(['custom', 'konami'], function(custom, konami, app) {
	var fileName  = 'header';
  	custom.logger(fileName + "Controller: Init");
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			custom.logger(fileName + "Controller: Loaded");
			
				app.controller(fileName + 'Controller', function($scope, $timeout, $rootScope, psResponsive, $localStorage, $sessionStorage, $detection, stBlurredDialog, $modal, toaster, uiModalCtrl, accountModalCtrl) {	   
				   $scope.fileName = fileName;
				  

				  

					///////////////////////	  INIT
					$scope.pageStatus = {
						isLoading: true,
						hasData: '',						
						hasErrors: false,	
						errorMsg: ''										
					};
					
				    $scope.defaultSettings = $localStorage.$default({
   							 language: "english"
					});					

					$scope.init = function(){
				  		$scope.userData = [];		
				  		
						$scope.defaultLanguage = $scope.defaultSettings.language;
						custom.parseFile("files/exampleSpreadSheet.csv", function(data){						
							$scope.languageObj = data[$scope.defaultLanguage];
							$scope.$apply();
						});						  		
				  				  		  		
				  		custom.checkUserData(function(returnState, data){	
				  			$scope.pageStatus.isLoading = false; 
				  			$scope.pageStatus.hasData = returnState; 
				  			
				  			
		  			
				  			
				  			// IF RETURNING USER DATA
				  			if (returnState){					  							  							  			
				  				$scope.userData = data.user;
				  				$scope.userImage = data.image;				  				
				  				$scope.$apply();	
				  			}else{				  								  			
				  				$scope.pageStatus.hasErrors  = data[0];
				  				$scope.pageStatus.errorMsg  = data[1];
				  				if (data[0]){$scope.$apply();};
				  			} 	 						  			
				  		});
				  		
			  			// GET LOGSTATE
			  			 custom.fetchLogState(function(state){				  			 		
			  			 	$scope.logState = state;				  			 		
			  			 });				  		
			  		
					};
					///////////////////////	 
					

				 	// ---------------- LOGIN
					$scope.openLogin = function (callback) {						
					
						var modalInstance = $modal.open({
					      	templateUrl: 'loginModal.html',
					  		controller: accountModalCtrl.loginModalCtrl(),
						});
						// VIA CLOSE
						modalInstance.result.then(function(returnData)
						{							 
										  							 		
						}, 
						// VIA DISMISS
						function (){
							
					  		
						});	
						
					};
					//-----------------		


				  	// DETECT AND APPLY STICKY IF IT WORKS
				  	if ($detection.isAndroid()){
				  		$scope.stickyWorks = true;
				  	}
				  	else if($detection.isiOS()){				  		
				  		$scope.stickyWorks = false;
				  	}
				  	else if($detection.isWindowsPhone()){
				  		$scope.stickyWorks = true;
				  	}
				  	else{				  		
				  		// PC OR DESKTOP
				  		$scope.stickyWorks = true;
				  	}
				   
				    $rootScope.responsive = psResponsive;
					$scope.offcanvasToggle = function(){
						 stBlurredDialog.open();	
							custom.offcanvas('toggle');
					};
				   
				   

					
				});			
				
				



				
								
					
	    },
	    ///////////////////////////////////////
  };
});
